package net.techU.controllers;

import net.techU.models.Cuenta;
import net.techU.services.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("cuentas")
public class CuentaController {
    private final CuentaService cuentaService;
    private Cuenta cuenta;

    @Autowired
    public  CuentaController (CuentaService cuentaService){
        this.cuentaService = cuentaService;
    }
    @GetMapping()
    public ResponseEntity<List<Cuenta>> cuentas(){
        System.out.println("Voy a traer la lista de Cuentas:");
        return ResponseEntity.ok(cuentaService.findAll());
    }
    @PostMapping()
    public void saveCuenta(@RequestBody Cuenta cue){
       cuentaService.saveCuenta(cue);
    }
}
