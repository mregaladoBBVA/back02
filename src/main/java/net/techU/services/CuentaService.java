package net.techU.services;

import net.techU.models.Cuenta;

import java.util.List;

public interface CuentaService {
    //Se define un método para listar todos los elementos y otro para una búsqueda individual
    List<Cuenta> findAll();
    //Se definen métodos de actualización de Vehículos para añadir, actualizar y eliminar
    public void saveCuenta(Cuenta cue);
}
