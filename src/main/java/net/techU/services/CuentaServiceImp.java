package net.techU.services;

import net.techU.models.Cuenta;
import net.techU.repository.CuentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("CuentaService")
@Transactional
public class CuentaServiceImp implements CuentaService{
    private CuentaRepository cuentaRepository;

    @Autowired
    public CuentaServiceImp(CuentaRepository cuentaRepository){
        this.cuentaRepository = cuentaRepository;
    }
    @Override
    public List<Cuenta> findAll() {
        return  cuentaRepository.findAll();
    }

    @Override
    public void saveCuenta(Cuenta cue) {
        cuentaRepository.saveCuenta(cue);
    }

}
