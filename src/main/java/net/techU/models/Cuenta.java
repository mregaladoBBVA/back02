package net.techU.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "cuentas")
//Se indica el orden de las propiedades del JSON a guardar
@JsonPropertyOrder({"idCuenta","Titular","numero_cuenta","moneda","saldo"})
public class Cuenta implements Serializable {
   private String idCuenta;
   private String Titular;
   private String numero_cuenta;
   private String moneda;
   private float saldo;

   public Cuenta(){}

   public Cuenta( String idCuenta, String Titular, String numero_cuenta,String moneda, float saldo){
       this.setIdCuenta(idCuenta);
       this.setTitular(Titular);
       this.setNumero_cuenta(numero_cuenta);
       this.setSaldo(saldo);
   }
    public String getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(String idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getTitular() {
        return Titular;
    }

    public void setTitular(String titular) {
        Titular = titular;
    }

    public String getNumero_cuenta() {
        return numero_cuenta;
    }

    public void setNumero_cuenta(String numero_cuenta) {
        this.numero_cuenta = numero_cuenta;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
}
