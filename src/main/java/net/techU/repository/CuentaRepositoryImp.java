package net.techU.repository;

import net.techU.models.Cuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CuentaRepositoryImp implements CuentaRepository{
    // Propiedades de la Clase
    private final MongoOperations mongoOperations;
    //  Para indicar que sea el primer método en ser ejecutado
    @Autowired
    public CuentaRepositoryImp(MongoOperations mongoOperations){
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Cuenta> findAll() {
        // Se obtiene una lista con todos los vehículos de la colección
        List<Cuenta> cuentas = this.mongoOperations.find(new Query(), Cuenta.class);
        return cuentas;
    }

    @Override
    public void saveCuenta(Cuenta cue) {
        this.mongoOperations.save(cue);
    }

}
