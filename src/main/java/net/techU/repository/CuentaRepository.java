package net.techU.repository;

import net.techU.models.Cuenta;

import java.util.List;

public interface CuentaRepository {
    //Se define un método para listar todos los elementos y otro para una búsqueda individual
    List<Cuenta> findAll();
    //Se definen métodos de actualización de Vehículos para añadir, actualizar y eliminar
    public void saveCuenta(Cuenta cue);
}
